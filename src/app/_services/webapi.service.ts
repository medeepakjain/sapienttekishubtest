import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebapiService {

    constructor(
        private httpClient: HttpClient
    ) { }
  
    /*
    * Model: Launches
    * Action: All Launches
    */
    launchesIndex(limit: number) {
        return this.httpClient.get<any> (
            `launches?limit=`+limit
        );
    }

    /*
    * Model: Launches
    * Action: Launch Success Filter
    */
    launchesFilters(limit: number, launch_success?: any, land_success?: any, launch_year?: any) {
        return this.httpClient.get<any> (
            `launches?limit=`+limit+`&launch_success=`+launch_success+`&land_success=`+land_success+`&launch_year=`+launch_year+``
        );
    }

}
