import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
    } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";

@Injectable()
export class InterceptorService implements HttpInterceptor {
    
    constructor(
    ) {
    }
    
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        let reqUrl = 'https://api.spacexdata.com/v3/';
        req = req.clone({
            url: reqUrl + "" + req.url
        });
        return next.handle(req);
    }
}
