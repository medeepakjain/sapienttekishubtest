import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { WebapiService } from './_services/webapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  title = 'Sapient - XT Coding Assignment | Interviewee : Deepak Jain';

  launch_years = [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020];

  filters = {
    year: null,
    launch: null,
    landing: null
  };

  spacex_programs = [];

  constructor(
    private webapi: WebapiService,
    private location: Location,
    private route: ActivatedRoute
  ) {
    if(this.route.snapshot.paramMap.get('launch_success')){
      this.filters.launch = this.route.snapshot.paramMap.get('launch_success');
    }
    if(this.route.snapshot.paramMap.get('land_success')){
      this.filters.landing = this.route.snapshot.paramMap.get('land_success');
    }
    if(this.route.snapshot.paramMap.get('launch_year')){
      this.filters.year = this.route.snapshot.paramMap.get('launch_year');
    }
  }

  ngOnInit(): void {
    // initialize default data
    if(
      (this.route.snapshot.paramMap.get('launch_success') == 'true') ||
      (this.route.snapshot.paramMap.get('launch_success') == 'false') ||
      (this.route.snapshot.paramMap.get('land_success') == 'true') ||
      (this.route.snapshot.paramMap.get('land_success') == 'false') ||
      this.route.snapshot.paramMap.get('launch_year')
    ) {
      this.onFiltersUpdate();
    } else {
      this.initializePrograms();
    }
  }

  initializePrograms() {
    this.webapi.launchesIndex(100)
      .subscribe(
        response => {
          if(response) {
            this.spacex_programs = response;
            return this.spacex_programs;
          }
        },
        error => {
          console.log(error);
          alert('An error occurred while fetching data from server');
        }
      );
  }

  onFiltersUpdate() {
    this.location.replaceState(
      '?launch_success=' + (this.filters.launch == null ? '' : this.filters.launch)
       + '&land_success=' + (this.filters.landing == null ? '' : this.filters.landing)
       + '&launch_year=' + (this.filters.year == null ? '' : this.filters.year)
    );
    this.webapi.launchesFilters(
      100,
      (this.filters.launch == null) ? '' : this.filters.launch,
      (this.filters.landing == null) ? '' : this.filters.landing,
      (this.filters.year == null) ? '' : this.filters.year
    )
      .subscribe(
        response => {
          if(response) {
            this.spacex_programs = response;
            return this.spacex_programs;
          }
        },
        error => {
          console.log(error);
          alert('An error occurred while fetching data from server');
        }
      );
  }

}
